using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Metrics
{
    public class NarrowToCellMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && (bool) value ? new Thickness(2, 1, 2, 1) : new Thickness(3, 1.5, 3, 1.5);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}