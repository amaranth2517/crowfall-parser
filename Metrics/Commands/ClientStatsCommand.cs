using System.IO;
using Metrics.Extensions;
using Metrics.Models;

namespace Metrics.Commands
{
    public class ClientStatsCommand : Command
    {
        public Fight RaidFight { get; set; }
        public string RaidName { get; set; }
        protected override Commands Id => Commands.ClientStats;
        public override bool UseRelay => false;
        public override bool AllowRelay => true;

        public ClientStatsCommand(Fight raidFight, string raidName)
        {
            RaidName = raidName;
            RaidFight = raidFight;

            // if (Character?.RaidFight != null)
            //     Character.RaidFight.SyncTime = syncTime;
        }

        public ClientStatsCommand()
        {
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(string.IsNullOrEmpty(RaidName) ? "Anonymous" : RaidName);
            RaidFight.Serialize(writer);
        }

        public override void Deserialize(BinaryReader reader)
        {
            RaidName = reader.ReadString();
            RaidFight = reader.Deserialize<Fight>();
        }

        public RaidCharacter ToRaidCharacter()
        {
            return new RaidCharacter
            {
                Name = RaidName,
                Fight = RaidFight.ToRaidFight()
            };
        }
    }
}