namespace Metrics.Commands
{
    public enum Commands : byte
    {
        Join = 0,
        Leave,
        ClientStats,
        Raid,
        RaidStats,
        IncompatibleProtocol,
        KeepAlive,
        Punch,
        Reconnect
    }
}