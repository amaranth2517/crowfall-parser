using System.IO;

namespace Metrics.Commands
{
    public class PunchingCommand : Command
    {
        public bool Punched { get; set; }
        protected override Commands Id => Commands.Punch;
        public override bool UseRelay => false;
        public override bool AllowRelay => false;

        public PunchingCommand(bool punched)
        {
            Punched = punched;
        }

        public PunchingCommand()
        {
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(Punched);
        }

        public override void Deserialize(BinaryReader reader)
        {
            Punched = reader.ReadBoolean();
        }

        public override string ToString()
        {
            return base.ToString() + $"[{Punched}]";
        }
    }
}