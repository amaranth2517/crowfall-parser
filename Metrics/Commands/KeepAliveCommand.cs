using System.IO;

namespace Metrics.Commands
{
    public class KeepAliveCommand : Command
    {
        protected override Commands Id => Commands.KeepAlive;
        public override bool UseRelay => false;
        public override bool AllowRelay => false;

        public override void Serialize(BinaryWriter writer)
        {
        }

        public override void Deserialize(BinaryReader reader)
        {
        }
    }
}