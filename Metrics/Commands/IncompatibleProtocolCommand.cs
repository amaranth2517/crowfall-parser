using System.IO;

namespace Metrics.Commands
{
    public class IncompatibleProtocolCommand : Command
    {
        public int ProtocolVersion { get; set; }
        protected override Commands Id => Commands.IncompatibleProtocol;
        public override bool UseRelay => true;
        public override bool AllowRelay => true;

        public IncompatibleProtocolCommand(int protocolVersion)
        {
            ProtocolVersion = protocolVersion;
        }

        public IncompatibleProtocolCommand()
        {
            
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(ProtocolVersion);
        }

        public override void Deserialize(BinaryReader reader)
        {
            ProtocolVersion = reader.ReadInt32();
        }
    }
}