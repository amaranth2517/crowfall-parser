using System.IO;

namespace Metrics.Commands
{
    public class JoinCommand : Command
    {
        public int ProtocolVersion { get; set; }
        protected override Commands Id => Commands.Join;
        public override bool UseRelay => true;
        public override bool AllowRelay => true;


        public JoinCommand(int protocolVersion)
        {
            ProtocolVersion = protocolVersion;
        }

        public JoinCommand()
        {
        }

        public override void Serialize(BinaryWriter writer)
        {
            writer.Write(ProtocolVersion);
        }

        public override void Deserialize(BinaryReader reader)
        {
            ProtocolVersion = reader.ReadInt32();
        }
    }
}