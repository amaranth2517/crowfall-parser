using System;
using System.IO;
using Metrics.Models;

namespace Metrics.Commands
{
    public abstract class Command : INetworkObject
    {
        protected abstract Commands Id { get; }
        public abstract bool UseRelay { get; }
        public abstract bool AllowRelay { get; }

        private int _size;

        public byte[] Serialize()
        {
            using var memoryStream = new MemoryStream();
            using (var writer = new BinaryWriter(memoryStream))
            {
                writer.Write((byte) Id);
                Serialize(writer);
            }

            var bytes = memoryStream.ToArray();
            _size = bytes.Length;

            return bytes;
        }

        public static Command Deserialize(byte[] data)
        {
            using var m = new MemoryStream(data);
            using var reader = new BinaryReader(m);

            Command command = (Commands) reader.ReadByte() switch
            {
                Commands.Join => new JoinCommand(),
                Commands.ClientStats => new ClientStatsCommand(),
                Commands.Raid => new RaidCommand(),
                Commands.RaidStats => new RaidStatsCommand(),
                Commands.IncompatibleProtocol => new IncompatibleProtocolCommand(),
                Commands.Punch => new PunchingCommand(),
                _ => null
            };

            command?.Deserialize(reader);
            command?.SetSize(data.Length);

            return command;
        }

        private void SetSize(int length)
        {
            _size = length;
        }

        public abstract void Serialize(BinaryWriter writer);

        public abstract void Deserialize(BinaryReader reader);

        public override string ToString()
        {
            return $"{Enum.GetName(typeof(Commands), Id) ?? "Unknown"}[{_size}]";
        }
    }
}