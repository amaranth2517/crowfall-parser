﻿using System.Diagnostics;

namespace Metrics
{
    public static class WindowsHelper
    {
        public static string Cmd(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"") + " && exit";

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "cmd",
                    Arguments = $"/K \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };

            process.Start();
            var result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            
            return result;
        }
    }
}