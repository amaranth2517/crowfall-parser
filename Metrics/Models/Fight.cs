using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Metrics.Extensions;
using OptionsModule;
using Prism.Mvvm;

namespace Metrics.Models
{
    public class Fight : BindableBase, INetworkObject
    {
        public int Index { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? LastAction { get; set; }
        public SortableObservableCollection<Skill> DamageDoneRecords { get; } = new SortableObservableCollection<Skill>();
        public SortableObservableCollection<Skill> HealingDoneRecords { get; } = new SortableObservableCollection<Skill>();
        public SortableObservableCollection<Skill> DamageReceivedRecords { get; } = new SortableObservableCollection<Skill>();
        public SortableObservableCollection<Skill> HealingReceivedRecords { get; } = new SortableObservableCollection<Skill>();
        public Skill DamageDone { get; set; } = new Skill();
        public Skill DamageReceived { get; set; } = new Skill();
        public Skill HealingDone { get; set; } = new Skill();
        public Skill HealingReceived { get; set; } = new Skill();
        public MaximumSkill MaxDamageDone { get; } = new MaximumSkill();
        public MaximumSkill MaxHealingDone { get; } = new MaximumSkill();
        public MaximumSkill MaxDamageReceived { get; } = new MaximumSkill();
        public MaximumSkill MaxHealingReceived { get; } = new MaximumSkill();

        public double IncomingCriticalRate => (double) IncomingHits > 0 ? IncomingCrits / (double) IncomingHits * 100 : 0;
        public SortableObservableCollection<Opponent> Opponents { get; } = new SortableObservableCollection<Opponent>();
        public ConcurrentStack<(CombatLog, string, DateTime)> Lines { get; } = new ConcurrentStack<(CombatLog, string, DateTime)>();
        public TimeSpan SyncTime { get; set; } = TimeSpan.Zero;

        public double Duration => LastAction == null || Started == null || LastAction < Started ? 0 : (LastAction.Value - Started.Value).TotalMilliseconds;
        public double CriticalRate => (double) Hits > 0 ? Crits / (double) Hits * 100 : 0;
        private int Crits => (DamageDone?.Crits ?? 0) + (HealingDone?.Crits ?? 0);
        private int IncomingCrits => DamageReceived?.Crits ?? 0;
        private int Hits => (DamageDone?.Hits ?? 0) + (HealingDone?.Hits ?? 0);
        private int IncomingHits => DamageReceived?.Hits ?? 0;
        public DateTime StartedOnServer { get; set; }

        private void UpdatePerSecond()
        {
            UpdatePerSecond(DamageDoneRecords);
            UpdatePerSecond(HealingDoneRecords);
            UpdatePerSecond(DamageReceivedRecords);
            UpdatePerSecond(HealingReceivedRecords);
            DamageDone?.UpdatePerSecond(Duration);
            HealingDone?.UpdatePerSecond(Duration);
            DamageReceived?.UpdatePerSecond(Duration);
            HealingReceived?.UpdatePerSecond(Duration);
        }

        private void UpdateDamageDone(string skill, string target, int amount, int absorbed, bool critical, bool selfDamage, string type)
        {
            UpdateSkills(DamageDoneRecords, DamageDone, skill, amount, absorbed, critical, type);
            UpdateOpponents(target, amount, absorbed, 0, 0, selfDamage);
            UpdateMaxStat(MaxDamageDone, amount, skill, target);
        }

        private void UpdateMaxStat(MaximumSkill stat, int amount, string skill, string target)
        {
            if (amount <= stat.Amount)
                return;

            stat.Update(amount, skill, target);
        }

        private void UpdateOpponents(string name, int amountDone, int amountDoneAbsorbed, int amountReceived, int amountReceivedAbsorbed, bool selfDamage)
        {
            var opponent = Opponents.FirstOrDefault(x => x.Name == name);
            if (opponent == null)
            {
                Opponents.Add(new Opponent(name, amountDone, amountReceived, amountDoneAbsorbed, amountReceivedAbsorbed, selfDamage));
                RaisePropertyChanged(nameof(Opponents));
            }
            else
            {
                opponent.DamageDone += amountDone;
                opponent.DamageReceived += amountReceived;
                opponent.DamageDoneAbsorbed += amountDoneAbsorbed;
                opponent.DamageReceivedAbsorbed += amountReceivedAbsorbed;
                Opponents.Reposition(opponent);
            }
        }

        private void UpdateHealingDone(string skill, string target, int amount, int absorbed, bool critical)
        {
            UpdateSkills(HealingDoneRecords, HealingDone, skill, amount, absorbed, critical, "hp");
            UpdateMaxStat(MaxHealingDone, amount, skill, target);
        }

        private void UpdateDamageReceived(string skill, string opponent, int amount, int absorbed, bool critical, bool selfDamage, string type)
        {
            UpdateSkills(DamageReceivedRecords, DamageReceived, skill, amount, absorbed, critical, type);
            UpdateOpponents(opponent, 0, 0, amount, absorbed, selfDamage);
            UpdateMaxStat(MaxDamageReceived, amount, skill, opponent);
        }

        private void UpdateHealingReceived(string skill, string opponent, int amount, int absorbed, bool critical)
        {
            UpdateSkills(HealingReceivedRecords, HealingReceived, skill, amount, absorbed, critical, "hp");
            UpdateMaxStat(MaxHealingReceived, amount, skill, opponent);
        }

        private void UpdatePerSecond(SortableObservableCollection<Skill> skills)
        {
            foreach (var skill in skills)
            {
                skill.UpdatePerSecond(Duration);
            }
        }

        private void UpdateSkills(ICollection<Skill> skills, Skill totalRecord, string name, int amount, int absorbed, bool critical, string type)
        {
            var collection = skills as SortableObservableCollection<Skill>;
            var skill = skills.FirstOrDefault(x => x.Name == name);
            if (skill == null)
                if (collection == null)
                    skills.Add(new Skill(name, amount, absorbed, critical, type));
                else
                    collection.Add(new Skill(name, amount, absorbed, critical, type));
            else
            {
                skill.AddHit(amount, absorbed, critical);
                collection?.Reposition(skill);
            }

            if (totalRecord == null)
                SetProperty(ref totalRecord, new Skill());

            totalRecord.AddHit(amount, absorbed, critical);
        }

        public void Update(bool mine, bool damage, string skill, string opponent, int amount, int absorbed, bool critical, bool me, bool selfDamage, string type)
        {
            if (mine)
            {
                if (damage)
                {
                    UpdateDamageDone(skill, opponent, amount, absorbed, critical, selfDamage, type);

                    if (me)
                        UpdateDamageReceived(skill, opponent, amount, absorbed, critical, selfDamage, type);
                }
                else
                {
                    UpdateHealingDone(skill, opponent, amount, absorbed, critical);

                    if (me)
                        UpdateHealingReceived(skill, opponent, amount, absorbed, critical);
                }
            }
            else
            {
                if (damage)
                    UpdateDamageReceived(skill, opponent, amount, absorbed, critical, selfDamage, type);
                else
                    UpdateHealingReceived(skill, opponent, amount, absorbed, critical);
            }

            UpdatePerSecond();
        }

        public static void EnsureComparison(SortableObservableCollection<Skill> collection, string direction, string property)
        {
            Comparison<Skill> comparison = (x, y) => 0;
            var sign = Options.GetCompareSign(direction);
            switch (property)
            {
                case "Total":
                case "PerSecond":
                    comparison = (x, y) => sign * y.Total.CompareTo(x.Total);
                    break;
                case "Hits":
                    comparison = (x, y) => sign * y.Hits.CompareTo(x.Hits);
                    break;
                case "Crits":
                    comparison = (x, y) => sign * y.Crits.CompareTo(x.Crits);
                    break;
                case "Name":
                    comparison = (x, y) => sign * string.Compare(y.Name, x.Name, StringComparison.InvariantCulture);
                    break;
            }

            collection.EnsureComparison(comparison);
        }

        public static void EnsureComparison(SortableObservableCollection<Opponent> collection, string direction, string property)
        {
            Comparison<Opponent> comparison = (x, y) => 0;
            var sign = Options.GetCompareSign(direction);
            switch (property)
            {
                case "DamageReceived":
                    comparison = (x, y) => sign * y.DamageReceived.CompareTo(x.DamageReceived);
                    break;
                case "DamageDone":
                    comparison = (x, y) => sign * y.DamageDone.CompareTo(x.DamageDone);
                    break;
                case "Name":
                    comparison = (x, y) => sign * string.Compare(y.Name, x.Name, StringComparison.InvariantCulture);
                    break;
            }

            collection.EnsureComparison(comparison);
        }

        public void EnsureComparisons()
        {
            EnsureComparison(DamageDoneRecords, Options.Inst.DamageDetailsSortingDirection, Options.Inst.DamageDetailsSortingProperty);
            EnsureComparison(HealingDoneRecords, Options.Inst.HealingDetailsSortingDirection, Options.Inst.HealingDetailsSortingProperty);
            EnsureComparison(Opponents, Options.Inst.OpponentsSortingDirection, Options.Inst.OpponentsSortingProperty);
            EnsureComparison(DamageReceivedRecords, Options.Inst.DamageDetailsSortingDirection, Options.Inst.DamageDetailsSortingProperty);
            EnsureComparison(HealingReceivedRecords, Options.Inst.HealingDetailsSortingDirection, Options.Inst.HealingDetailsSortingProperty);
        }

        public void BufferLine(CombatLog log, string line, DateTime timestamp)
        {
            Lines.Push((log, line, timestamp));

            for (var i = 0; i < Lines.Count - 100; i++)
            {
                Lines.TryPop(out _);
            }
        }

        public void Serialize(BinaryWriter writer)
        {
            writer.WriteDateTime(LastAction - SyncTime);
            if (Options.Inst.DontSendMyMetrics)
                DamageDone.SerializeEmpty(writer);
            else
                DamageDone.Serialize(writer);
            DamageReceived.Serialize(writer);
            if (Options.Inst.DontSendMyMetrics)
                HealingDone.SerializeEmpty(writer);
            else
                HealingDone.Serialize(writer);
            HealingReceived.Serialize(writer);
            writer.SerializeCollection(Opponents);
        }

        public void Deserialize(BinaryReader reader)
        {
            LastAction = reader.ReadDateTime();
            DamageDone = reader.Deserialize<Skill>();
            DamageReceived = reader.Deserialize<Skill>();
            HealingDone = reader.Deserialize<Skill>();
            HealingReceived = reader.Deserialize<Skill>();
            // todo: no need for sorting
            reader.DeserializeCollection<Opponent>().ForEach(x => Opponents.Add(x));
        }

        public RaidFight ToRaidFight()
        {
            return new RaidFight
            {
                DamageDone = DamageDone,
                DamageReceived = DamageReceived,
                HealingDone = HealingDone,
                HealingReceived = HealingReceived,
                Opponents = Opponents
            };
        }
    }
}