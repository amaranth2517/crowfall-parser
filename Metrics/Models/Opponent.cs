using System.IO;
using Prism.Mvvm;

namespace Metrics.Models
{
    public class Opponent : BindableBase, IRecord, INetworkObject
    {
        private int _damageDone;
        private int _damageDoneAbsorbed;
        private int _damageReceived;
        private int _damageReceivedAbsorbed;
        private int _order;

        public Opponent(string name, int damageDone, int damageReceived, int damageDoneAbsorbed, int damageReceivedAbsorbed, bool selfDamage = false)
        {
            Name = name;
            DamageDone = damageDone;
            DamageReceived = damageReceived;
            DamageDoneAbsorbed = damageDoneAbsorbed;
            DamageReceivedAbsorbed = damageReceivedAbsorbed;
            SelfDamage = selfDamage;
        }

        public Opponent()
        {
        }

        public string Name { get; set; }

        public int DamageDone
        {
            get => _damageDone;
            set => SetProperty(ref _damageDone, value);
        }

        public int DamageReceived
        {
            get => _damageReceived;
            set => SetProperty(ref _damageReceived, value);
        }

        public int DamageDoneAbsorbed
        {
            get => _damageDoneAbsorbed;
            set => SetProperty(ref _damageDoneAbsorbed, value);
        }

        public int DamageReceivedAbsorbed
        {
            get => _damageReceivedAbsorbed;
            set => SetProperty(ref _damageReceivedAbsorbed, value);
        }

        public int Order
        {
            get => _order;
            set => SetProperty(ref _order, value);
        }

        public int Percent { get; set; }
        public bool SelfDamage { get; set; }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(DamageDone);
            writer.Write(DamageReceived);
            writer.Write(Name);
        }

        public void Deserialize(BinaryReader reader)
        {
            DamageDone = reader.ReadInt32();
            DamageReceived = reader.ReadInt32();
            Name = reader.ReadString();
        }
    }
}