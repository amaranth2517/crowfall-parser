using Prism.Mvvm;

namespace Metrics.Models
{
    public class MaximumSkill : BindableBase
    {
        private int _amount;
        private string _skill;
        private string _target;

        public int Amount
        {
            get => _amount;
            private set => SetProperty(ref _amount, value);
        }

        public string Skill
        {
            get => _skill;
            private set => SetProperty(ref _skill, value);
        }

        public string Target
        {
            get => _target;
            private set => SetProperty(ref _target, value);
        }

        public void Update(int amount, string skill, string target)
        {
            Amount = amount;
            Skill = skill;
            Target = target;
        }
    }
}