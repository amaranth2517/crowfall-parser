using System.Collections.Generic;
using System.Linq;
using Prism.Mvvm;

namespace Metrics.Models
{
    public class RaidInfo : BindableBase
    {
        private List<string> _members;
        private string _leader;
        private int _idleDuration;

        public List<string> Members
        {
            get => _members;
            set => SetProperty(ref _members, value);
        }

        public string Leader
        {
            get => _leader;
            set => SetProperty(ref _leader, value);
        }

        public int IdleDuration
        {
            get => _idleDuration;
            set => SetProperty(ref _idleDuration, value);
        }


        public override string ToString()
        {
            return $"Leader: {Leader} [{IdleDuration} secs] \nMembers: {string.Join(", ", Members.Where(x => x != Leader))}";
        }
    }
}