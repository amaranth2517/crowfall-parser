using System.Windows.Media;
using OptionsModule;
using Prism.Mvvm;

namespace Metrics.Models
{
    public class SkillListModel : BindableBase
    {
        public SkillListModel(string totalLabel, string perSecondLabel, StatType statType)
        {
            PerSecondLabel = perSecondLabel;
            TotalLabel = totalLabel;
            StatType = statType;

            Options.Inst.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(Options.Inst.HealingDoneColor):
                    case nameof(Options.Inst.DamageDoneColor):
                        RaisePropertyChanged(nameof(PaintColor));
                        break;
                }
            };
        }

        private SortableObservableCollection<Skill> _skills;

        public SortableObservableCollection<Skill> Skills
        {
            get => _skills;
            set => SetProperty(ref _skills, value);
        }

        public string PerSecondLabel { get; set; }
        public string TotalLabel { get; set; }

        public Color PaintColor => StatType == StatType.Heal ? Options.Inst.HealingDoneColor : (StatType == StatType.Damage ? Options.Inst.DamageDoneColor : Options.Inst.DamageReceivedColor);
        public StatType StatType { get; }
    }

    public enum StatType
    {
        Damage,
        Heal,
        RecievedDamage
    }
}