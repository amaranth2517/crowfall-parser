using System.IO;
using Metrics.Extensions;
using Prism.Mvvm;

namespace Metrics.Models
{
    public sealed class Skill : BindableBase, IRecord, INetworkObject
    {
        private int _crits;
        private int _hits;
        private int _perSecond;
        private int _total;
        private int _absorbed;
        private string _type;

        public Skill()
        {
        }

        public Skill(string name, int total, int absorbed, bool critical, string type)
        {
            Name = name;
            Total = total;
            Type = type;
            Absorbed = absorbed;
            PerSecond = 0;
            Hits = 1;
            Crits = critical ? 1 : 0;
        }

        public string Type
        {
            get => _type;
            set => SetProperty(ref _type, value);
        }

        public int Absorbed
        {
            get => _absorbed;
            set => SetProperty(ref _absorbed, value);
        }

        public string Name { get; }

        public int PerSecond
        {
            get => _perSecond;
            set => SetProperty(ref _perSecond, value);
        }

        public int Total
        {
            get => _total;
            set => SetProperty(ref _total, value);
        }

        public int Hits
        {
            get => _hits;
            set => SetProperty(ref _hits, value);
        }

        public int Crits
        {
            get => _crits;
            set => SetProperty(ref _crits, value);
        }

        public void AddHit(int amount, int absorbed, bool critical)
        {
            Total += amount;
            Absorbed += absorbed;
            Hits++;

            if (critical)
                Crits++;
        }

        public void UpdatePerSecond(double duration)
        {
            PerSecond = Total.CalcPerSecond(duration);
        }

        public int Order { get; set; }
        public int Percent { get; set; }

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(PerSecond);
            writer.Write(Total);
        }

        // note: pass serialisation options instead if it gets bigger
        public void SerializeEmpty(BinaryWriter writer)
        {
            writer.Write(0);
            writer.Write(0);
        }

        public void Deserialize(BinaryReader reader)
        {
            PerSecond = reader.ReadInt32();
            Total = reader.ReadInt32();
        }
    }
}