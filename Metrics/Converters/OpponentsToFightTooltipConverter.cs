using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Metrics.Models;

namespace Metrics.Converters
{
    public class OpponentsToFightTooltipConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ICollection<Opponent> collection)
                return string.Join(", ", collection.OrderByDescending(x => x.DamageDone + x.DamageReceived).Select(x => x.Name));

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}