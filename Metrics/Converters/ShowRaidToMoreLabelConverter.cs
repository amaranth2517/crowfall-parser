using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Metrics.Converters
{
    public class ShowRaidToMoreLabelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && (Visibility) value == Visibility.Visible ? "⮝ Raid" : "⮟ Raid";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}