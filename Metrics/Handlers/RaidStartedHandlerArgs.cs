using System;

namespace Metrics.Handlers
{
    public delegate void RaidStartedHandler(RaidStartedHandlerArgs args);

    public class RaidStartedHandlerArgs
    {
        public DateTime Started { get; set; }
        public DateTime StartedOnServer { get; set; }
    }
}