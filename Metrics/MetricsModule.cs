﻿using Metrics.Services;
using Metrics.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace Metrics
{
    public class MetricsModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("ContentRegion", typeof(MainControl));
            regionManager.RegisterViewWithRegion("RaidContentRegion", typeof(SkillList));
            regionManager.RegisterViewWithRegion("DamageSkills", typeof(SkillList));
            regionManager.RegisterViewWithRegion("HealSkills", typeof(SkillList));
            regionManager.RegisterViewWithRegion("ReceivedDamageSkills", typeof(SkillList));
            regionManager.RegisterViewWithRegion("ReceivedHealSkills", typeof(SkillList));
            regionManager.RegisterViewWithRegion("Settings", typeof(SettingsControl));
            regionManager.RegisterViewWithRegion("Raid", typeof(RaidControl));
            regionManager.RegisterViewWithRegion("Hotkeys", typeof(HotkeysControl));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton(typeof(ServerService));
            containerRegistry.RegisterSingleton(typeof(ClientService));
            containerRegistry.RegisterSingleton(typeof(FightService));
            containerRegistry.RegisterSingleton(typeof(WebService));
            containerRegistry.RegisterSingleton(typeof(HttpService));
            containerRegistry.RegisterSingleton(typeof(AfkNotifierService));
            containerRegistry.RegisterSingleton(typeof(NotificationService));
        }
    }
}