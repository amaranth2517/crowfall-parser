using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Windows;
using AsyncAwaitBestPractices.MVVM;
using Metrics.Controls;
using Metrics.Models;
using Metrics.Services;
using Metrics.Views;
using NLog;
using OptionsModule;
using Prism.Commands;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class RaidControlViewModel : BindableBase
    {
        private readonly WebService _webService;
        private readonly HttpService _httpService;
        private readonly FightService _fightService;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private bool _raidJoined;
        private bool _raidButtonEnabled;
        private string _raidName;
        private List<RaidWindowBase> _raidWindows = new List<RaidWindowBase>();

        private RaidInfo _raidInfo;
        private long _raidFightOrder = 1;
        private long _raidFightsCount;
        private ICollection<Opponent> _raidFightOpponents;

        public ICollection<Opponent> RaidFightOpponents
        {
            get => _raidFightOpponents;
            private set => SetProperty(ref _raidFightOpponents, value);
        }

        public bool RaidJoined
        {
            get => _raidJoined;
            private set => SetProperty(ref _raidJoined, value);
        }

        public bool RaidButtonEnabled
        {
            get => _raidButtonEnabled;
            private set => SetProperty(ref _raidButtonEnabled, value);
        }

        public string RaidName
        {
            get => _raidName;
            set => SetProperty(ref _raidName, value);
        }


        public RaidInfo RaidInfo
        {
            get => _raidInfo;
            set => SetProperty(ref _raidInfo, value);
        }

        public long RaidFightOrder
        {
            get => _raidFightOrder;
            set => SetProperty(ref _raidFightOrder, value);
        }

        public long RaidFightsCount
        {
            get => _raidFightsCount;
            set => SetProperty(ref _raidFightsCount, value);
        }

        public DelegateCommand PreviousRaidFightCommand { get; }

        public DelegateCommand NextRaidFightCommand { get; }

        public DelegateCommand LastRaidFightCommand { get; }

        public DelegateCommand FirstRaidFightCommand { get; }

        public AsyncCommand ToggleRaidCommand { get; }

        public RaidControlViewModel(WebService webService, HttpService httpService, FightService fightService, ClientService clientService)
        {
            _webService = webService;
            _httpService = httpService;
            _fightService = fightService;

            var getStats = new Subject<string>();
            getStats.Throttle(TimeSpan.FromMilliseconds(600)).Subscribe(async _ => await GetRaidStats());

            _webService.MembersInfoUpdated += (sender, info) =>
            {
                RaidInfo = info;
                Logger.Info(RaidInfo);
            };

            _webService.FightsCountUpdated += (sender, count) =>
            {
                RaidFightsCount = count;
                Logger.Info($"FIGHTS COUNT: {count}");
            };

            clientService.StatsReceived += args => RaidFightOpponents = args.Opponents;

            _webService.NewFightStarted += (sender, e) =>
            {
                if (_fightService.RaidFight == null)
                    return;

                RaidFightsCount++;
                if (RaidFightsCount - 1 == RaidFightOrder)
                {
                    RaidFightOrder++;
                    RaidFightOpponents = null;
                }
            };

            _fightService.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(_fightService.CurrentCharacter):

                        if (_fightService.CurrentCharacter != null)
                        {
                            RaidName = _fightService.CurrentCharacter.Log?.Name;
                            RaidButtonEnabled = true;
                        }

                        break;
                }
            };

            Observable.FromEventPattern<EventHandler, EventArgs>(h => _webService.LostConnection += h, h => _webService.LostConnection -= h)
                .ObserveOnDispatcher().Subscribe(next => LeaveRaid());

            ToggleRaidCommand = new AsyncCommand(OnToggleRaid);

            FirstRaidFightCommand = new DelegateCommand(() =>
            {
                if (RaidFightOrder > 1)
                {
                    RaidFightOrder = 1;
                    getStats.OnNext("");
                }
            });
            LastRaidFightCommand = new DelegateCommand(() =>
            {
                if (RaidFightOrder < RaidFightsCount)
                {
                    RaidFightOrder = RaidFightsCount;
                    getStats.OnNext("");
                }
            });
            PreviousRaidFightCommand = new DelegateCommand(() =>
            {
                if (RaidFightOrder > 1)
                {
                    RaidFightOrder--;
                    getStats.OnNext("");
                }
            });
            NextRaidFightCommand = new DelegateCommand(() =>
            {
                if (RaidFightOrder < RaidFightsCount)
                {
                    RaidFightOrder++;
                    getStats.OnNext("");
                }
            });
        }

        private async Task GetRaidStats()
        {
            var raidStatsCommand = await _httpService.GetRaidStats(RaidFightOrder - 1);
            if (raidStatsCommand != null)
            {
                RaidFightOpponents = raidStatsCommand.Opponents;
                _webService.ShowRaidStats(raidStatsCommand);
            }
            else
            {
                RaidFightOpponents = null;
                Logger.Warn($"Fight {RaidFightOrder} is not found in {Options.Inst.RaidHandle}");
            }

            Options.Inst.LastRaidFight = RaidFightOrder == RaidFightsCount;
        }

        private async Task OnToggleRaid()
        {
            if (!RaidJoined)
            {
                if (await TogglingRaid(async () => await _webService.Join(Options.Inst.RaidHandle, RaidName)))
                {
                    JoinRaid();
                    await NavigateToLastFight();
                    await GetRaidStats();
                }
            }
            else
            {
                if (await TogglingRaid(async () => await _webService.Leave()))
                {
                    LeaveRaid();
                }
            }
        }


        private async Task NavigateToLastFight()
        {
            RaidFightsCount = await _httpService.GetFightsCount();
            RaidFightOrder = RaidFightsCount;
        }

        private void JoinRaid()
        {
            RaidJoined = true;
            Options.Inst.PropertyChanged += InstOnPropertyChanged;
            _raidWindows = new List<RaidWindowBase> {new RaidOpponentsWindow(), new DamageDoneRaidWindow(), new HealingDoneRaidWindow(), new DamageReceivedRaidWindow()};
            foreach (var raidWindow in _raidWindows.Where(x => Options.Inst.GetRaidVisibility(x.ViewMode)))
            {
                raidWindow.Owner = Application.Current.MainWindow;
                raidWindow.Show();
            }

            _fightService.RaidName = RaidName;
        }

        private void LeaveRaid()
        {
            Logger.Info("LEFT RAID");
            RaidJoined = false;
            Options.Inst.PropertyChanged -= InstOnPropertyChanged;
            _raidWindows.ForEach(x => x.Close());
        }

        private async Task<bool> TogglingRaid(Func<Task<bool>> action)
        {
            RaidButtonEnabled = false;
            var result = await action();
            RaidButtonEnabled = _fightService.CurrentCharacter != null;

            return result;
        }

        private void InstOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var window = _raidWindows.FirstOrDefault(x => e.PropertyName == "Show" + x.ViewMode);
            if (window == null)
                return;

            if (Options.Inst.GetRaidVisibility(window.ViewMode))
                window.Show();
            else
                window.Hide();
        }
    }
}