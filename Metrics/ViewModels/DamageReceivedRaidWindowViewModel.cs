using Metrics.Models;
using Metrics.Services;
using OptionsModule;

namespace Metrics.ViewModels
{
    public class DamageReceivedRaidWindowViewModel : RaidWindowViewModel
    {
        public DamageReceivedRaidWindowViewModel(ClientService clientService) : base(clientService)
        {
            Options.Inst.PropertyChanged += (sender, args) =>
            {
                switch (args.PropertyName)
                {
                    case nameof(Options.Inst.RaidDamageReceivedSortingDirection):
                    case nameof(Options.Inst.RaidDamageReceivedSortingProperty):
                        EnsureComparison();
                        break;
                }
            };
        }
        
        protected override void UpdatePlayer(RaidPlayer ch, RaidCharacter character)
        {
            ch.Total = character.Fight.DamageReceived.Total;
            ch.PerSecond = character.Fight.DamageReceived.PerSecond;
        }

        protected override  RaidPlayer NewPlayer(RaidCharacter character)
        {
            return new RaidPlayer(character.Name, character.Fight.DamageReceived.Total, character.Fight.DamageReceived.PerSecond);
        }
        
        protected override string GetProperty()
        {
            return Options.Inst.RaidDamageReceivedSortingProperty;
        }

        protected override string GetDirection()
        {
            return Options.Inst.RaidDamageReceivedSortingDirection;
        }

        protected override bool AddPlayer(RaidCharacter character)
        {
            return character.Fight.DamageReceived?.Total > 0;
        }
    }
}