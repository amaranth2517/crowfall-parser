using System.Collections.ObjectModel;
using OptionsModule;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class HotkeysControlViewModel : BindableBase
    {
        private ObservableCollection<HotkeyBinding> _hotkeysBindings;

        public HotkeysControlViewModel()
        {
            HotkeysBindings = new ObservableCollection<HotkeyBinding>
            {
                Options.Inst.GetHotkeyBinding(Hotkeys.Windows),
                Options.Inst.GetHotkeyBinding(Hotkeys.More),
                Options.Inst.GetHotkeyBinding(Hotkeys.Raid),
                Options.Inst.GetHotkeyBinding(Hotkeys.AfkNotifier)
            };
        }

        public ObservableCollection<HotkeyBinding> HotkeysBindings
        {
            get => _hotkeysBindings;
            set => SetProperty(ref _hotkeysBindings, value);
        }
    }
}