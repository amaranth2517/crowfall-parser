using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Metrics.Services;
using NLog;
using Prism.Commands;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    public class SendBugReportWindowViewModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly HttpService _httpService;
        private readonly FightService _fightService;

        private string _submitLabel = "Send";
        public DelegateCommand SendBugReportCommand { get; }

        public string Comment { get; set; }

        public bool SendLogs { get; set; } = true;

        public bool SendInfo { get; set; } = true;

        public string SubmitLabel
        {
            get => _submitLabel;
            private set => SetProperty(ref _submitLabel, value);
        }

        public string DiscordHandle { get; set; }

        public SendBugReportWindowViewModel(HttpService httpService, FightService fightService)
        {
            _httpService = httpService;
            _fightService = fightService;
            SendBugReportCommand = new DelegateCommand(OnSendBugReport);
        }

        private async void OnSendBugReport()
        {
            var form = new MultipartFormDataContent {{new StringContent(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion), "version"}};

            if (_fightService.CurrentCharacter != null)
                form.Add(new StringContent(_fightService.CurrentCharacter.Log.Name), "character");

            if (DiscordHandle != null)
                form.Add(new StringContent(DiscordHandle), "discordHandle");

            if (Comment != null)
                form.Add(new StringContent(Comment), "comment");

            if (SendLogs)
            {
                SubmitLabel = "Attaching logs...";
                await Task.Run(() =>
                {
                    var directoryInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Crowfall Parser\Logs");
                    if (directoryInfo.Exists)
                    {
                        var i = 1;
                        foreach (var file in directoryInfo.GetFiles().OrderByDescending(x => x.LastWriteTime).Take(5))
                        {
                            form.Add(new ByteArrayContent(File.ReadAllBytes(file.FullName)), $"log{i}", file.Name);
                            i++;
                        }
                    }
                });
            }

            if (SendInfo)
            {
                SubmitLabel = "Gathering system info...";
                await Task.Run(() =>
                {
                    var infoBuilder = new StringBuilder();
                    infoBuilder.AppendLine("systeminfo".Cmd());
                    infoBuilder.AppendLine("ipconfig /all".Cmd());
                    var systemInfo = infoBuilder.ToString();
                    infoBuilder.AppendLine(string.Format(systemInfo));
                    form.Add(new StringContent(systemInfo), "systemInfo");
                });
            }


            SubmitLabel = "Sending bug report...";

            try
            {
                var result = await _httpService.PostAsync("api/bug-report", form);

                SubmitLabel = result.IsSuccessStatusCode ? "Successfully sent. Thank you!" : "Send failed. Please contact me on Discord: Amaranth#9305";
            }
            catch (Exception e)
            {
                Logger.Error(e, "COULD NOT SEND BUG REPORT");
                SubmitLabel = "Send failed. Please contact me on Discord: Amaranth#9305";
            }
        }
    }
}