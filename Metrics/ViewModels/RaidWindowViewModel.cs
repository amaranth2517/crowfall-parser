using System;
using System.Linq;
using System.Reactive.Linq;
using Metrics.Handlers;
using Metrics.Models;
using Metrics.Services;
using OptionsModule;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    public abstract class RaidWindowViewModel : BindableBase
    {
        public bool FromHistory { get; set; }
        protected abstract string GetProperty();

        protected abstract string GetDirection();

        public SortableObservableCollection<RaidPlayer> Characters { get; } = new SortableObservableCollection<RaidPlayer>();

        protected RaidWindowViewModel(ClientService clientService)
        {
            EnsureComparison();

            Observable.FromEvent<StatsReceivedHandler, StatsReceivedHandlerArgs>(h1 => clientService.StatsReceived += h1, h2 => clientService.StatsReceived -= h2)
                .ObserveOnDispatcher().Subscribe(e =>
                {
                    FromHistory = e.FromHistory;
                    // todo: implement Remove
                    if (e.FromHistory || Characters.Any(x => e.Characters.All(y => y.Name != x.Name)))
                        Characters.Clear();

                    foreach (var character in e.Characters)
                    {
                        if (!AddPlayer(character))
                            continue;

                        var ch = Characters.FirstOrDefault(x => x.Name == character.Name);
                        if (ch == null)
                        {
                            Characters.Add(NewPlayer(character));
                        }
                        else
                        {
                            UpdatePlayer(ch, character);
                            Characters.Reposition(ch);
                        }
                    }
                });

            Observable.FromEvent<RaidStartedHandler, RaidStartedHandlerArgs>(h1 => clientService.RaidStarted += h1, h2 => clientService.RaidStarted -= h2)
                .ObserveOnDispatcher().Subscribe(e =>
                {
                    if (!FromHistory)
                        Characters.Clear();
                });
        }


        protected abstract void UpdatePlayer(RaidPlayer ch, RaidCharacter character);

        protected abstract RaidPlayer NewPlayer(RaidCharacter character);

        protected void EnsureComparison()
        {
            var sign = Options.GetCompareSign(GetDirection());
            var property = GetProperty();
            var comparison = GetComparison(sign, property);

            Characters.EnsureComparison(comparison);
        }

        private static Comparison<RaidPlayer> GetComparison(int sign, string property)
        {
            Comparison<RaidPlayer> comparison = property switch
            {
                "Total" => (x, y) => sign * y.Total.CompareTo(x.Total),
                "PerSecond" => (x, y) => sign * y.PerSecond.CompareTo(x.PerSecond),
                _ => (x, y) => 0
            };

            return comparison;
        }


        protected abstract bool AddPlayer(RaidCharacter character);
    }
}