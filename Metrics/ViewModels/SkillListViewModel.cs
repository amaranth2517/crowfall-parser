using Metrics.Models;
using Prism.Mvvm;

namespace Metrics.ViewModels
{
    public class SkillListViewModel : BindableBase
    {
        private SkillListModel _model;

        public SkillListModel Model
        {
            get => _model;
            set => SetProperty(ref _model, value);
        }
    }
}