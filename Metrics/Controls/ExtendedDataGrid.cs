using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Metrics.Services;
using OptionsModule;

namespace Metrics.Controls
{
    public class ExtendedDataGrid : DataGrid
    {
        public static readonly DependencyProperty PaintPropertyProperty = DependencyProperty.Register(
            "PaintProperty", typeof(string), typeof(ExtendedDataGrid), new PropertyMetadata(default(string)));

        public static readonly DependencyProperty PaintColorProperty = DependencyProperty.Register(
            "PaintColor", typeof(Color), typeof(ExtendedDataGrid), new PropertyMetadata(Colors.MidnightBlue));

        public static readonly DependencyProperty PaintModeProperty = DependencyProperty.Register(
            "PaintMode", typeof(PaintMode), typeof(ExtendedDataGrid), new PropertyMetadata(PaintMode.DontPaint));

        public static readonly DependencyProperty NarrowProperty = DependencyProperty.Register(
            "Narrow", typeof(bool), typeof(ExtendedDataGrid), new PropertyMetadata(default(bool)));

        public bool Narrow
        {
            get => (bool) GetValue(NarrowProperty);
            set => SetValue(NarrowProperty, value);
        }
        
        public Color PaintColor
        {
            get => (Color) GetValue(PaintColorProperty);
            set => SetValue(PaintColorProperty, value);
        }

        public string PaintProperty
        {
            get => (string) GetValue(PaintPropertyProperty);
            set => SetValue(PaintPropertyProperty, value);
        }

        public PaintMode PaintMode
        {
            get => (PaintMode) GetValue(PaintModeProperty);
            set => SetValue(PaintModeProperty, value);
        }

        public ExtendedDataGrid()
        {
            IsReadOnly = true;
            EnableRowVirtualization = true;
            AutoGenerateColumns = false;
            SelectionChanged += (sender, args) => SelectAll();
            HeadersVisibility = DataGridHeadersVisibility.Column;
            RowBackground = Brushes.Transparent;
            HorizontalGridLinesBrush = Brushes.Transparent;
            MouseRightButtonDown += (sender, args) => Focus();
            VerticalGridLinesBrush = Brushes.Transparent;
            CanUserReorderColumns = false;
            CanUserResizeColumns = false;
            ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

            var contextMenu = new ContextMenu();
            contextMenu.Items.Add(new MenuItem {Header = "Copy", Command = ApplicationCommands.Copy});
            ContextMenu = contextMenu;

            FightService.Repaint.ObserveOnDispatcher().Subscribe(mode =>
            {
                if (mode == PaintMode)
                    UpdateGraph();
            });

            Sorting += (sender, e) =>
            {
                e.Column.SortDirection = e.Column.SortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;
                e.Handled = true;
            };

            AddHandler(ScrollViewer.ScrollChangedEvent, new ScrollChangedEventHandler(OnScrollChanged));

            NameScope.SetNameScope(this, new NameScope());

            Options.Inst.PropertyChanged += OnSettingChanged;
            Unloaded += (sender, args) => Options.Inst.PropertyChanged -= OnSettingChanged;
        }

        private void OnSettingChanged(object sender, PropertyChangedEventArgs args)
        {
            switch (args.PropertyName)
            {
                case nameof(Options.Inst.HealingDoneColor):
                case nameof(Options.Inst.DamageDoneColor):
                case nameof(Options.Inst.DamageReceivedColor):
                    UpdateGraph();
                    break;
            }
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            UpdateGraph();
        }

        protected override void OnSorting(DataGridSortingEventArgs eventArgs)
        {
            base.OnSorting(eventArgs);
            UpdateGraph();
        }

        // private static int counter = 0;

        // todo: it is possible to do it with binding, check performance with enabled visualization
        private void UpdateGraph()
        {
            if (PaintProperty == null)
                return;

            UpdateLayout();

            var max = 0;
            var sum = 0d;

            var dict = new Dictionary<object, int>();
            foreach (var item in Items)
            {
                var value = GetPropertyValue(item);
                dict.Add(item, value);

                if (value > max)
                    max = value;

                sum += value;
            }

            foreach (var pair in dict)
            {
                var item = ItemContainerGenerator.ContainerFromIndex(Items.IndexOf(pair.Key));
                if (item is DataGridRow row)
                {
                    row.ToolTip = $"{pair.Value / sum * 100:#0.##}%";
                    var offset = Math.Round(max > 0 ? (double) pair.Value / max : 0, 2);

                    if (row.Background is LinearGradientBrush linearGradientBrush)
                    {
                        #region AnimationDoesNotLookGood

                        // counter++;
                        // var oldOffset = linearGradientBrush.GradientStops[0].Offset;
                        // this.RegisterName("GradientStop1" + counter, linearGradientBrush.GradientStops[0]);
                        // this.RegisterName("GradientStop2" + counter, linearGradientBrush.GradientStops[1]);
                        //
                        // var offsetAnimation = new DoubleAnimation
                        // {
                        //     From = oldOffset,
                        //     To = offset,
                        //     Duration = TimeSpan.FromMilliseconds(100)
                        // };
                        //
                        // Storyboard.SetTargetName(offsetAnimation, "GradientStop1" + counter);
                        // Storyboard.SetTargetProperty(offsetAnimation, new PropertyPath(GradientStop.OffsetProperty));
                        //
                        // var offsetAnimation2 = new DoubleAnimation
                        // {
                        //     From = oldOffset,
                        //     To = offset,
                        //     Duration = TimeSpan.FromMilliseconds(100)
                        // };
                        //
                        // Storyboard.SetTargetName(offsetAnimation2, "GradientStop2" + counter);
                        // Storyboard.SetTargetProperty(offsetAnimation2, new PropertyPath(GradientStop.OffsetProperty));
                        //
                        // new Storyboard {Children = new TimelineCollection {offsetAnimation, offsetAnimation2}}.Begin(this);

                        #endregion

                        var bar = linearGradientBrush.GradientStops[0];
                        bar.Offset = offset;
                        if (bar.Color != PaintColor)
                            bar.Color = PaintColor;

                        linearGradientBrush.GradientStops[1].Offset = offset;
                    }
                    else
                    {
                        row.Background = new LinearGradientBrush
                        {
                            GradientStops = new GradientStopCollection
                            {
                                new GradientStop {Color = PaintColor, Offset = offset},
                                new GradientStop {Color = Colors.Transparent, Offset = offset},
                            },
                            StartPoint = new Point(0, 0.5d),
                            EndPoint = new Point(1, 0.5d)
                        };
                    }
                }
            }
        }

        private int GetPropertyValue(object item)
        {
            var type = item?.GetType();
            item = type?.GetProperty(PaintProperty)?.GetValue(item);

            return int.TryParse(item?.ToString(), out var result) ? result : 0;
        }
    }
}