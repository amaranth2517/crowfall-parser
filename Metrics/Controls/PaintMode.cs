namespace Metrics.Controls
{
    public enum PaintMode
    {
        OnTick,
        OnStatsReceived,
        DontPaint
    }
}