﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using OptionsModule;

namespace Metrics.Views
{
    public partial class HealingDoneRaidWindow
    {
        public HealingDoneRaidWindow()
        {
            InitializeComponent();
            
            SetBinding(TopProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.TopHealingDone)), Delay = 3000, Mode = BindingMode.TwoWay});
            SetBinding(LeftProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.LeftHealingDone)), Delay = 3000, Mode = BindingMode.TwoWay});
        }
        
        private void HealingDone_OnSorting(object sender, DataGridSortingEventArgs e)
        {
            Options.Inst.RaidHealingDoneSortingProperty = e.Column.SortMemberPath;
            Options.Inst.RaidHealingDoneSortingDirection = e.Column.SortDirection.ToString();
        }

        public override RaidViewMode ViewMode => RaidViewMode.HealingDone;
    }
}