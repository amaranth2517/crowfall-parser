﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Metrics.Controls;
using Metrics.Models;
using Metrics.ViewModels;
using OptionsModule;
using Prism.Common;
using Prism.Regions;

namespace Metrics.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SkillList
    {
        public SkillList()
        {
            InitializeComponent();
            RegionContext.GetObservableContext(this).PropertyChanged += SkillList_PropertyChanged;
        }

        private void SkillList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var context = (ObservableObject<object>) sender;
            var model = (SkillListModel) context.Value;
            ((SkillListViewModel) DataContext).Model = model;
            switch (model.StatType)
            {
                case StatType.Heal:
                    Skills.SetBinding(ExtendedDataGrid.PaintPropertyProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.HealingDetailsSortingProperty))});
                    break;
                case StatType.Damage:
                    Skills.SetBinding(ExtendedDataGrid.PaintPropertyProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.DamageDetailsSortingProperty))});
                    break;
                default:
                    Skills.SetBinding(ExtendedDataGrid.PaintPropertyProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.DamageDetailsSortingProperty))});
                    break;
            }

            RegionContext.GetObservableContext(this).PropertyChanged -= SkillList_PropertyChanged;
        }

        private void SkillList_OnSorting(object sender, DataGridSortingEventArgs e)
        {
            var statType = ((SkillListViewModel) DataContext).Model.StatType;
            if (statType == StatType.Heal)
            {
                Options.Inst.HealingDetailsSortingProperty = e.Column.SortMemberPath;
                Options.Inst.HealingDetailsSortingDirection = e.Column.SortDirection.ToString();
            }
            else
            {
                Options.Inst.DamageDetailsSortingProperty = e.Column.SortMemberPath;
                Options.Inst.DamageDetailsSortingDirection = e.Column.SortDirection.ToString();
            }
        }
    }
}