﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using OptionsModule;

namespace Metrics.Views
{
    public partial class DamageDoneRaidWindow
    {
        public DamageDoneRaidWindow()
        {
            InitializeComponent();

            SetBinding(TopProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.TopDamageDone)), Delay = 3000, Mode = BindingMode.TwoWay});
            SetBinding(LeftProperty, new Binding {Source = Options.Inst, Path = new PropertyPath(nameof(Options.Inst.LeftDamageDone)), Delay = 3000, Mode = BindingMode.TwoWay});
        }

        private void DamageDone_OnSorting(object sender, DataGridSortingEventArgs e)
        {
            Options.Inst.RaidDamageDoneSortingProperty = e.Column.SortMemberPath;
            Options.Inst.RaidDamageDoneSortingDirection = e.Column.SortDirection.ToString();
        }

        public override RaidViewMode ViewMode => RaidViewMode.DamageDone;
    }
}