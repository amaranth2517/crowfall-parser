using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Metrics.Commands;
using Metrics.Models;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using OptionsModule;
using STUN;

namespace Metrics.Services
{
    public sealed class WebService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ServerService _serverService;
        private readonly ClientService _clientService;
        private readonly NotificationService _notificationService;
        private HubConnection _connection;
        private STUNQueryResult _stunResult;
        private bool _isServer;
        private string _raidHandle;
        private string _raidName;
        private ParserClient _serverClient;
        private ParserClient _clientClient;

        // private const string WebUrl = "http://127.0.0.1/raid";
        private const string WebUrl = "https://sugoi-bot.herokuapp.com/raid";

        public event EventHandler LostConnection;
        public event EventHandler<RaidInfo> MembersInfoUpdated;
        public event EventHandler<long> FightsCountUpdated;

        public EventHandler<EventArgs> NewFightStarted { get; set; }

        public WebService(ServerService serverService, ClientService clientService, NotificationService notificationService)
        {
            _serverService = serverService;
            _clientService = clientService;
            _notificationService = notificationService;

            Options.Inst.PropertyChanged += async (sender, args) =>
            {
                if (args.PropertyName == nameof(Options.Inst.IdleDuration) && _isServer)
                {
                    await SendIdleAsync();
                }
            };

            Application.Current.Exit += async (sender, args) =>
            {
                Options.Inst.ProgramLaunches++;
                await Leave();
            };
        }

        private static async Task<STUNQueryResult> QueryStun()
        {
            var retriesCount = 0;
            while (true)
            {
                try
                {
                    var ipv6 = retriesCount > 4;

                    var client = new UdpClient(AddressFamily.InterNetworkV6) {Client = {DualMode = true}};
                    client.AllowNatTraversal(true);

                    if (!StunClient.TryParseHostAndPort("stun.ekiga.net:3478", out IPEndPoint stunEndPoint, ipv6))
                        throw new Exception("Failed to resolve STUN server address");

                    var queryResult = await StunClient.QueryAsync(client.Client, stunEndPoint, STUNQueryType.ExactNAT);

                    if (queryResult.QueryError != STUNQueryError.Success)
                        throw new Exception("Query Error: " + queryResult.QueryError);

                    queryResult.PublicEndPoint.Address = queryResult.PublicEndPoint.Address.MapToIPv6();
                    queryResult.LocalEndPoint.Address = IPAddress.Loopback.MapToIPv6();

                    Logger.Info("PublicEndPoint: {0}", queryResult.PublicEndPoint);
                    Logger.Info("LocalEndPoint: {0}", queryResult.LocalEndPoint);
                    Logger.Info("NAT Type: {0}", queryResult.NATType);

                    await Task.Run(() =>
                    {
                        //note: keeps public endpoint alive
                        // todo: dispose as soon as connection is closed
                        var timer = new Timer(sender =>
                        {
                            try
                            {
                                client.Send(new byte[] {1}, 1, stunEndPoint);
                            }
                            catch (Exception e)
                            {
                                ((Timer) sender).Dispose();
                                Logger.Error(e, "PINGING STUN CRASHED");
                            }
                        });
                        timer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(10));
                    });

                    return queryResult;
                }
                catch (Exception e)
                {
                    retriesCount++;
                    Logger.Error(e, "FAILED TO COMPLETE STUN. RETRYING...");
                    await Task.Delay(200);

                    if (retriesCount > 10)
                        throw new Exception("FAILED TO DISCOVER PUBLIC IP");
                }
            }
        }

        public async Task<bool> Join(string raidHandle, string raidName)
        {
            try
            {
                _raidHandle = raidHandle;
                _raidName = raidName;
                _stunResult = await QueryStun();

                _connection = new HubConnectionBuilder().AddJsonProtocol(x =>
                {
                    x.PayloadSerializerOptions.Converters.Add(new IPAddressConverter());
                    x.PayloadSerializerOptions.Converters.Add(new IPEndPointConverter());
                }).WithUrl(WebUrl).Build();
                _connection.ServerTimeout = TimeSpan.FromSeconds(10);
                _connection.KeepAliveInterval = TimeSpan.FromSeconds(2);
                _connection.Closed += ConnectionOnClosed;
                _connection.Reconnected += ConnectionOnReconnected;

                _connection.On<string>("Connected", OnConnected);
                _connection.On<IPEndPoint>("ServerInfo", async info => await OnServerInfo(info));
                _connection.On<List<Computer>>("TakeOver", OnTakeOver);
                _connection.On<Computer>("Joining", OnJoining);
                _connection.On<Computer>("Leaving", OnLeaving);
                _connection.On<byte[], IPEndPoint>("Relay", OnRelay);
                _connection.On<RaidInfo>("Members", OnMembers);
                _connection.On<IPEndPoint>("Rejoin", OnRejoin);
                _connection.On<long>("FightsUpdated", OnFightsUpdated);

                await _connection.StartAsync();
                await _connection.SendAsync("Join", _raidHandle, _stunResult.PublicEndPoint, _raidName);
                _notificationService.ShowSuccess($"Connected to Raid {_raidHandle}");

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "EXCEPTION CONNECTING TO WEB");
                return false;
            }
        }

        private async Task OnRejoin(IPEndPoint endPoint)
        {
            _clientService.Reset();
            await _clientService.JoinServer(endPoint);
        }

        private void OnFightsUpdated(long fightsCount)
        {
            FightsCountUpdated?.Invoke(this, fightsCount);
        }

        private void OnMembers(RaidInfo raidInfo)
        {
            MembersInfoUpdated?.Invoke(this, raidInfo);
        }

        private static void OnConnected(string message)
        {
            Logger.Info($"CONNECTED TO WEB. Web time: {message}");
        }

        private async Task OnServerInfo(IPEndPoint serverEndPoint)
        {
            var startServer = Equals(_stunResult.PublicEndPoint, serverEndPoint);

            if (_clientService.AlreadyConnectedTo(serverEndPoint) || startServer && _isServer)
            {
                if (_isServer)
                    await SendIdleAsync();

                return;
            }

            try
            {
                _isServer = startServer;
                Logger.Info($"Server: {serverEndPoint} - {_isServer}");
                if (_isServer)
                {
                    //note: stop server if it was running
                    _serverClient?.Stop();
                    _serverClient = null;

                    if (_clientClient == null || _clientClient.IsLoopback())
                    {
                        // note: fresh start, setup server
                        _serverClient = new ParserClient(_stunResult.Socket, _serverService, this);
                        _serverClient.Listen();
                    }
                    else
                    {
                        // note: client is promoted to server
                        _clientClient.ChangeRaidService(_serverService);
                        _serverClient = _clientClient;
                        _clientClient = null;
                    }

                    if (_clientClient == null)
                    {
                        // note: localhost client socket 
                        _clientClient = new ParserClient(null, _clientService, this);
                        _clientClient.Listen();
                    }

                    await _clientService.JoinServer(_stunResult.LocalEndPoint);
                    await SendIdleAsync();
                }
                else
                {
                    if (_clientClient == null || _clientClient.IsLoopback())
                    {
                        if (_serverClient == null)
                        {
                            _clientClient?.Stop();
                            _clientClient = new ParserClient(_stunResult.Socket, _clientService, this);
                            _clientClient.Listen();
                        }
                        else
                        {
                            // note: server is demoted to client
                            _serverClient.ChangeRaidService(_clientService);
                            _clientClient = _serverClient;
                            _serverClient = null;
                        }
                    }

                    await _clientService.JoinServer(serverEndPoint);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "EXCEPTION JOINING RAID");
                await Leave();
                LostConnection?.Invoke(this, EventArgs.Empty);
                _notificationService.ShowError($"Failed to join Raid {_raidHandle}");
            }
        }

        private async Task SendIdleAsync()
        {
            await _connection.SendAsync("Idle", Options.Inst.IdleDuration);
        }

        private async void OnTakeOver(List<Computer> members)
        {
            await OnServerInfo(_stunResult.PublicEndPoint);
            await Task.WhenAll(members.Select(client => _serverService.PunchHole(client)));
        }

        private async void OnJoining(Computer member)
        {
            await _serverService.PunchHole(member);
        }

        private void OnLeaving(Computer computer)
        {
            try
            {
                _serverService.RemoveReceiver(computer.EndPoint);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"EXCEPTION REMOVING RECEIVER {computer.EndPoint}");
            }
        }

        private void OnRelay(byte[] data, IPEndPoint endPoint)
        {
            try
            {
                if (_isServer)
                    _serverClient?.HandleRelay(data, endPoint);
                else
                    _clientClient?.HandleRelay(data, endPoint);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"EXCEPTION RELAYING FROM {endPoint}");
            }
        }

        private async Task ConnectionOnClosed(Exception exception)
        {
            if (exception != null)
                Logger.Error(exception, "EXCEPTION CONNECTING TO WEB");

            const int retries = 6;
            for (var retry = 1; retry <= retries; retry++)
            {
                try
                {
                    await Task.Delay(retry * 400);
                    Logger.Warn($"RECONNECTING. TRY {retry}");
                    await _connection.StartAsync();
                    await Reconnect();
                    return;
                }
                catch (Exception e)
                {
                    Logger.Error(e, "EXCEPTION RECONNECTING TO WEB");
                }
            }

            LostConnection?.Invoke(this, EventArgs.Empty);
        }

        private async Task Reconnect()
        {
            await _connection.SendAsync("Reconnect", _raidHandle, _stunResult.PublicEndPoint, _raidName);
            _notificationService.ShowWarning($"Reconnected to Raid {_raidHandle}");
            Logger.Info("RECONNETED TO WEB.");
        }

        private async Task ConnectionOnReconnected(string arg)
        {
            await Reconnect();
        }

        public async Task<bool> Leave()
        {
            try
            {
                // todo: needs to be better
                if (_connection.State == HubConnectionState.Connected && _isServer)
                    await _serverService.UploadRaidStats();

                _serverClient?.Stop();
                _clientClient?.Stop();
                _serverClient = null;
                _clientClient = null;
                _isServer = false;

                _connection.Closed -= ConnectionOnClosed;
                if (_connection.State == HubConnectionState.Connected)
                {
                    await _connection.SendAsync("Leave");
                    await _connection.StopAsync();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "EXCEPTION CLOSING CONNECTION");
            }

            return true;
        }

        public async Task Relay(byte[] data, List<IPEndPoint> receivers)
        {
            await _connection.SendAsync("Relay", data, receivers);
        }

        public async Task ChangeServer()
        {
            await _connection.SendAsync("ChangeServer");
        }

        public async Task UpdConnectionLost()
        {
            Logger.Error("LOST CONNECTION TO SERVER");
            await Leave();
            LostConnection?.Invoke(this, EventArgs.Empty);
            _notificationService.ShowError($"Lost connection to Raid {_raidHandle}");
        }

        public async Task UploadRaidStats(DateTime started, DateTime ended, string name, RaidCharacter[] characters)
        {
            if (characters.Any())
                await _connection.SendAsync("UploadRaidStats", started, ended, name, characters);
        }

        public void ShowRaidStats(RaidStatsCommand raidStatsCommand)
        {
            _clientService.ShowRaidStats(raidStatsCommand, true);
        }

        public void StartNewFight()
        {
            NewFightStarted?.Invoke(this, EventArgs.Empty);
        }
    }
}