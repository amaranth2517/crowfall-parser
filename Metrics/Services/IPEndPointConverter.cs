using System;
using System.Globalization;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace Metrics.Services
{
    public class IPEndPointConverter : JsonConverter<IPEndPoint>
    {
        private static readonly Regex EndPointRegex = new Regex("^[[]?(.*?)[]]?:(\\d+)$", RegexOptions.Compiled | RegexOptions.Singleline);

        /// <inheritdoc/>
        public override IPEndPoint Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.String)
                throw new JsonException();

            var match = EndPointRegex.Match(reader.GetString());
            if (!match.Success)
                throw new JsonException();

            try
            {
                return new IPEndPoint(IPAddress.Parse(match.Groups[1].Value), int.Parse(match.Groups[2].Value, CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                throw new JsonException("Unexpected value format, unable to parse IPEndPoint.", ex);
            }
        }

        /// <inheritdoc/>
        public override void Write(Utf8JsonWriter writer, IPEndPoint value, JsonSerializerOptions options) => writer.WriteStringValue(value.ToString());
    }
}