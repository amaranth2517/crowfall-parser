using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Metrics.Commands;
using Metrics.Extensions;
using Metrics.Models;
using NLog;

namespace Metrics.Services
{
    public sealed class ParserClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private RaidService _raidService;
        private readonly WebService _webService;
        private UdpClient _client;
        private readonly ConcurrentDictionary<IPEndPoint, ConcurrentDictionary<ushort, Chunk[]>> _buffer;
        private CancellationTokenSource _cts;
        private int _socketErrors;
        private readonly bool _isLoopback;
        public const int ChunkSize = 1270;

        public ParserClient(Socket socket, RaidService raidService, WebService webService)
        {
            _raidService = raidService;
            _webService = webService;
            _socketErrors = 0;
            _buffer = new ConcurrentDictionary<IPEndPoint, ConcurrentDictionary<ushort, Chunk[]>>();

            try
            {
                _client = new UdpClient(AddressFamily.InterNetworkV6) {Client = {DualMode = true}};
                if (socket == null)
                {
                    _client.Client.Bind(new IPEndPoint(0, 0));
                    _isLoopback = true;
                }
                else
                {
                    _client.Client = socket;
                    _isLoopback = false;
                }
            }
            catch (Exception e)
            {
                LogError(e, "ERROR OPENING SOCKET");
            }
        }

        public long ReceivedCommands { get; private set; }
        public IPEndPoint EndPoint => (IPEndPoint) _client.Client.LocalEndPoint; 

        public void Listen()
        {
            LogInfo($"START LISTENING ON {_client.Client.LocalEndPoint}");
            _cts = new CancellationTokenSource();
            var token = _cts.Token;

            Task.Run(() => Receive(token), token);

            _raidService.SentCommand += OnSentCommand;
            _raidService.Start(_webService);
        }

        private async Task OnSentCommand(object sender, SentCommandHandlerArgs args)
        {
            await SendCommand(args.Receivers, args.Command);
        }

        private async Task SendByUdp(List<Receiver> receivers, Command command, byte[] data)
        {
            var receiversByUdp = receivers.Where(x => !ShouldBeRelayed(command, x));
            var sentByUdp = new List<IPEndPoint>();

            foreach (var receiver in receiversByUdp)
            {
                var chunks = CreateChunks(data, receiver.LastCommandId);
                foreach (var chunk in chunks)
                {
                    if (_client != null)
                        await _client?.SendAsync(chunk, chunk.Length, receiver.ToUdpEndPoint());
                }

                sentByUdp.Add(receiver.ToUdpEndPoint());
                receiver.CommandSent();
            }

            if (sentByUdp.Any())
            {
                LogInfo($"Send by UDP {command} to {sentByUdp.Print()}");
                _socketErrors = 0;
            }
        }

        private async Task Receive(CancellationToken token)
        {
            try
            {
                while (_client != null)
                {
                    var result = await _client.ReceiveAsync();
                    if (token.IsCancellationRequested)
                        break;

                    var chunk = ParseChunk(result.Buffer);
                    AddChunk(result.RemoteEndPoint, chunk);

                    var commandData = TryConstructCommand(result.RemoteEndPoint, chunk.CommandId);
                    if (commandData == null)
                        continue;

                    var command = Command.Deserialize(commandData);
                    if (command == null)
                        continue;

                    LogInfo($"Received by UDP {command} from {result.RemoteEndPoint}");
                    await _raidService.Process(command, result.RemoteEndPoint, true);
                    ClearCommandBuffer(result.RemoteEndPoint, chunk);
                }
            }
            catch (Exception e)
            {
                LogError(e, "RECEIVING LOOP CRASHED");
                await HandleSocketException(e);
            }
        }

        private void ClearCommandBuffer(IPEndPoint remoteEndPoint, Chunk chunk)
        {
            if (_buffer.ContainsKey(remoteEndPoint))
                _buffer[remoteEndPoint].RemoveSync(chunk.CommandId);
        }

        public void AddChunk(IPEndPoint remoteEndPoint, Chunk chunk)
        {
            var commands = _buffer.GetOrAddSync(remoteEndPoint, new ConcurrentDictionary<ushort, Chunk[]>());
            var chunks = commands.GetOrAddSync(chunk.CommandId, new Chunk[chunk.Count]);
            chunks[chunk.Order] = chunk;
        }

        public byte[] TryConstructCommand(IPEndPoint endPoint, ushort chunkCommandId)
        {
            using var stream = new MemoryStream();

            var binaryWriter = new BinaryWriter(stream);
            var chunks = _buffer[endPoint][chunkCommandId];

            for (var i = 0; i < chunks.Length; i++)
            {
                var chunk = chunks[i];
                if (chunk == null)
                    return null;

                stream.Position = i * ChunkSize;
                binaryWriter.Write(chunk.Data);
            }

            ReceivedCommands++;

            return stream.ToArray();
        }

        public async Task SendCommand(List<Receiver> receivers, Command command)
        {
            try
            {
                if (!receivers.Any())
                    return;

                var data = command.Serialize();
                await SendByRelay(receivers, command, data);
                await SendByUdp(receivers, command, data);
            }
            catch (Exception e)
            {
                LogError(e, $"SENDING {command} FAILED");
                await HandleSocketException(e);
            }
        }

        private async Task SendByRelay(IEnumerable<Receiver> receivers, Command command, byte[] data)
        {
            var receiversByRelay = receivers.Where(x => ShouldBeRelayed(command, x)).ToList();
            var sentByRelay = receiversByRelay.Select(x => x.ToRelayEndPoint()).ToList();
            if (sentByRelay.Any())
            {
                await _webService.Relay(data, sentByRelay);
                receiversByRelay.ForEach(x => x.CommandSent());

                if (sentByRelay.Any())
                    LogInfo($"Send by RELAY {command} to {sentByRelay.Print()}");
            }
        }

        private static bool ShouldBeRelayed(Command command, Receiver x)
        {
            return !x.IsLoopback() && (command.UseRelay || !x.ConnectedByUdp && command.AllowRelay);
        }

        public void HandleRelay(byte[] commandData, IPEndPoint remoteEndPoint)
        {
            var command = Command.Deserialize(commandData);
            if (command == null)
                return;

            LogInfo($"Received by RELAY {command} from {remoteEndPoint}");
            _raidService.Process(command, remoteEndPoint, false);
        }

        public void Stop()
        {
            LogInfo("CLOSING SOCKET");
            _client.Close();
            _cts?.Cancel();
            _client = null;
            _raidService.Reset();
            _raidService.SentCommand -= OnSentCommand;
        }

        private async Task HandleSocketException(Exception exception)
        {
            if (exception is SocketException socketException)
            {
                if (socketException.SocketErrorCode != SocketError.NetworkUnreachable)
                {
                    _socketErrors++;

                    // todo: change to event
                    if (_socketErrors > 4)
                        await _webService.UpdConnectionLost();
                }
            }
        }

        public static IEnumerable<byte[]> CreateChunks(byte[] data, ushort commandId)
        {
            using var outputStream = new MemoryStream();
            using var inputStream = new MemoryStream(data);
            var binaryReader = new BinaryReader(inputStream);
            var binaryWriter = new BinaryWriter(outputStream);
            var chunksCount = (byte) (data.Length / ChunkSize + 1);
            for (byte i = 0; i < chunksCount; i++)
            {
                binaryWriter.Write(commandId);
                binaryWriter.Write(i);
                binaryWriter.Write(chunksCount);
                binaryWriter.Write(binaryReader.ReadBytes(ChunkSize));
                var chunk = outputStream.ToArray();
                yield return chunk;
                outputStream.SetLength(0);
            }
        }

        public static Chunk ParseChunk(byte[] data)
        {
            using var inputStream = new MemoryStream(data);
            var binaryReader = new BinaryReader(inputStream);
            var chunk = new Chunk
            {
                CommandId = binaryReader.ReadUInt16(),
                Order = binaryReader.ReadByte(),
                Count = binaryReader.ReadByte(),
                Data = binaryReader.ReadBytes(ChunkSize)
            };

            return chunk;
        }

        private void LogInfo(string message)
        {
            Logger.Info($"{_raidService.ServiceType}: {message}");
        }

        private void LogError(Exception e, string message)
        {
            Logger.Error(e, $"{_raidService.ServiceType}: {message}");
        }

        public void ChangeRaidService(RaidService service)
        {
            _raidService.Reset();
            _raidService.SentCommand -= OnSentCommand;
            _raidService = service;
            _raidService.SentCommand += OnSentCommand;
            _raidService.Start(_webService);
        }

        public bool IsLoopback()
        {
            return _isLoopback;
        }

        public bool IsConnected()
        {
            return _client != null;
        }
    }
}