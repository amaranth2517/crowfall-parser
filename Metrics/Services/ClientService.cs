using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Metrics.Commands;
using Metrics.Controls;
using Metrics.Handlers;
using Metrics.Models;
using OptionsModule;

namespace Metrics.Services
{
    public sealed class ClientService : RaidService
    {
        public override string ServiceType => "CLIENT";

        private Receiver _server;
        private bool _punched;
        private Timer _keepAliveTimer;

        public event StatsReceivedHandler StatsReceived;

        public event RaidStartedHandler RaidStarted;

        // todo: wrong place
        public event EventHandler RaidStopped;
        public TimeSpan SyncTime;

        public async Task JoinServer(IPEndPoint endPoint)
        {
            _server = new Receiver(endPoint, true) {RelayPort = endPoint.Port};
            _punched = false;
            SyncTime = TimeSpan.Zero;

            if (!_server.IsLoopback())
            {
                for (var i = 0; i < PunchingTries; i++)
                {
                    LogInfo($"CONNECTING TO SERVER {endPoint}");
                    await Send(new PunchingCommand(_punched));
                    await Task.Delay(i * 500);
                }

                if (!_server.ConnectedByUdp)
                    LogInfo($"FAILED TO REACH SERVER {_server.ToUdpEndPoint()}, USE RELAY");
            }

            _keepAliveTimer = new Timer(async sender =>
            {
                try
                {
                    if (_server == null || !_server.ConnectedByUdp || _server.IsLoopback())
                        return;

                    if (_server.LostConnection() && WebRelay != null)
                        await WebRelay.UpdConnectionLost();
                    else
                        await Send(new KeepAliveCommand());
                }
                catch (Exception e)
                {
                    ((Timer) sender).Dispose();
                    LogError(e, "EXCEPTION SENDING KEEP ALIVE");
                }
            });
            _keepAliveTimer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(KeepAliveIntervalSecs));

            await Send(new JoinCommand(ProtocolVersion));
        }

        public override async Task Process(Command command, IPEndPoint endPoint, bool byUdp)
        {
            switch (command)
            {
                case KeepAliveCommand _:
                    _server.AliveAt = DateTime.Now;
                    break;
                case ReconnectCommand _:
                    if (WebRelay != null)
                        await WebRelay.UpdConnectionLost();
                    break;
                case PunchingCommand punchingCommand:
                    if (punchingCommand.Punched)
                        _server.ConnectedByUdp = true;

                    _server.UdpPort = endPoint.Port;
                    _punched = true;
                    break;
                case RaidCommand raidCommand:
                    if (raidCommand.ServerTime != null)
                    {
                        SyncTime = (DateTime.Now - raidCommand.ServerTime).Value;
                        LogInfo($"SYNCHRONISED TIME: {SyncTime}");
                    }

                    if (raidCommand.Started != null)
                    {
                        WebRelay?.StartNewFight();
                        RaidStarted?.Invoke(new RaidStartedHandlerArgs {Started = raidCommand.Started.Value + SyncTime, StartedOnServer = raidCommand.Started.Value});
                    }

                    break;
                case IncompatibleProtocolCommand incompatibleProtocolCommand:
                    var version = incompatibleProtocolCommand.ProtocolVersion > ProtocolVersion ? "newer" : "older";
                    var action = incompatibleProtocolCommand.ProtocolVersion > ProtocolVersion ? "update your" : "ask the Raid leader to update";
                    MessageBox.Show($"Your Raid leader uses {version} incompatible version. Please {action} {Options.Inst.Name} to newest compatible version.", $"Update {Options.Inst.Name}");
                    Reset();
                    break;
                case RaidStatsCommand raidStatsCommand:
                    if (Options.Inst.LastRaidFight)
                        ShowRaidStats(raidStatsCommand);
                    break;
            }
        }

        public void ShowRaidStats(RaidStatsCommand raidStatsCommand, bool fromHistory = false)
        {
            OnStatsReceived(new StatsReceivedHandlerArgs
            {
                Characters = raidStatsCommand.Characters,
                Opponents = raidStatsCommand.Opponents,
                FromHistory = fromHistory
            });
        }

        public async Task Send(Command command)
        {
            await SendCommand(new List<Receiver> {_server}, command);
        }

        public override void Reset()
        {
            RaidStopped?.Invoke(this, EventArgs.Empty);
            _server = null;
            _keepAliveTimer?.Dispose();
        }

        private void OnStatsReceived(StatsReceivedHandlerArgs args)
        {
            StatsReceived?.Invoke(args);
            FightService.Repaint.OnNext(PaintMode.OnStatsReceived);
        }

        public bool AlreadyConnectedTo(IPEndPoint serverEndPoint)
        {
            return Equals(_server?.ToRelayEndPoint(), serverEndPoint);
        }
    }
}