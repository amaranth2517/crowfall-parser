using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Metrics.Commands;
using Metrics.Models;
using NLog;
using OptionsModule;

namespace Metrics.Services
{
    public sealed class HttpService
    {
        private readonly HttpClient _client;

        public HttpService()
        {
            // _client = new HttpClient {BaseAddress = new Uri("http://127.0.0.1", UriKind.Absolute)};
            _client = new HttpClient {BaseAddress = new Uri("https://sugoi-bot.herokuapp.com", UriKind.Absolute)};
        }

        public Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content)
        {
            return _client.PostAsync(requestUri, content);
        }

        public async Task<RaidStatsCommand> GetRaidStats(long fightIndex)
        {
            var httpResponseMessage = await _client.GetAsync($"api/raids/{Options.Inst.RaidHandle}/{fightIndex}");
            if (!httpResponseMessage.IsSuccessStatusCode)
                return null;

            var response = await httpResponseMessage.Content.ReadAsStringAsync();
            var raidCharacters = JsonSerializer.Deserialize<List<RaidCharacter>>(response, new JsonSerializerOptions {PropertyNameCaseInsensitive = true});

            return new RaidStatsCommand(raidCharacters);
        }

        public async Task<int> GetFightsCount()
        {
            var httpResponseMessage = await _client.GetAsync($"api/raids/{Options.Inst.RaidHandle}/fights-count");
            if (!httpResponseMessage.IsSuccessStatusCode)
                return 0;

            var response = await httpResponseMessage.Content.ReadAsStringAsync();
            int.TryParse(response, out var result);

            return result;
        }

        public async Task UpdateVersion()
        {
            try
            {
                var version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
                var branch = File.ReadAllText(Directory.GetParent(Assembly.GetEntryAssembly()?.Location) + "/branch.txt");
                var httpResponseMessage = await _client.GetAsync($"api/publish/new-version/{branch}/{version}");

                if (!httpResponseMessage.IsSuccessStatusCode)
                    return;

                var url = await httpResponseMessage.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(url))
                    return;

                var response = await _client.GetAsync(url);
                var fileStream = await response.Content.ReadAsStreamAsync();

                var tempFile = Path.GetTempFileName();
                var stream = File.OpenWrite(tempFile);
                await fileStream.CopyToAsync(stream);
                fileStream.Close();
                stream.Close();
                File.Move(tempFile, tempFile + ".exe");

                Process.Start(tempFile + ".exe", "/SILENT");
                Application.Current.Shutdown();
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e);
            }
        }
    }
}