using System.Threading.Tasks;

namespace Metrics.Services
{
    public delegate Task SentCommandHandler(object sender, SentCommandHandlerArgs args);
}