using System;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace Metrics.Services
{
    public sealed class NotificationService
    {
        private readonly Notifier _notifier;

        public NotificationService()
        {
            _notifier = new Notifier(cfg =>
            {
                cfg.PositionProvider = new PrimaryScreenPositionProvider(
                    corner: Corner.BottomRight,
                    offsetX: 10,
                    offsetY: 10);

                cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                    notificationLifetime: TimeSpan.FromSeconds(4),
                    maximumNotificationCount: MaximumNotificationCount.FromCount(2));

                cfg.Dispatcher = Application.Current.Dispatcher;
            });
        }

        public void ShowError(string message)
        {
            ShowOnDispatcher(() => _notifier.ShowError(message));
        }

        public void ShowSuccess(string message)
        {
            ShowOnDispatcher(() => _notifier.ShowSuccess(message));
        }

        public void ShowWarning(string message)
        {
            ShowOnDispatcher(() => _notifier.ShowWarning(message));
        }

        private static void ShowOnDispatcher(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }
    }
}