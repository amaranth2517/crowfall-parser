using System.Diagnostics;
using System.Reflection;
using Metrics.Services;
using OptionsModule;
using Prism.Mvvm;

namespace Crowbars.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = $"{Options.Inst.Name} {FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion} by Amaranth";

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public MainWindowViewModel(HttpService httpService)
        {
            httpService.UpdateVersion();
        }
    }
}