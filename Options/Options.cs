using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using Prism.Mvvm;

namespace OptionsModule
{
    public class Options : BindableBase
    {
        private static Options _inst;
        private readonly IOptionsService _optionsService;
        public event EventHandler SaveError;
        private bool _topmost = true;

        private Options(IOptionsService optionsService)
        {
            _optionsService = optionsService;
        }

        public static Options Inst => _inst ?? (_inst = new Options(new IniParserService()));

        public double PrimaryOpacity
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(PrimaryOpacity), "0.5"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(PrimaryOpacity), value.ToString(CultureInfo.InvariantCulture));
        }

        public double SecondaryOpacity
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(SecondaryOpacity), "0.3"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(SecondaryOpacity), value.ToString(CultureInfo.InvariantCulture));
        }

        public Brush Foreground => new SolidColorBrush(ForegroundColor);

        public Color ForegroundColor
        {
            get => (Color?) ColorConverter.ConvertFromString(_optionsService.Get(Sections.Ui, nameof(ForegroundColor), "White")) ?? Colors.White;
            set
            {
                SaveOption(Sections.Ui, nameof(ForegroundColor), value.ToString());
                RaisePropertyChanged(nameof(Foreground));
            }
        }

        public Color BackgroundColor
        {
            get => (Color?) ColorConverter.ConvertFromString(_optionsService.Get(Sections.Ui, nameof(BackgroundColor), "Black")) ?? Colors.Black;
            set => SaveOption(Sections.Ui, nameof(BackgroundColor), value.ToString());
        }

        public Color DamageDoneColor
        {
            get => (Color?) ColorConverter.ConvertFromString(_optionsService.Get(Sections.Ui, nameof(DamageDoneColor), "#A41919C8")) ?? Color.FromArgb(164, 25, 25, 200);
            set => SaveOption(Sections.Ui, nameof(DamageDoneColor), value.ToString());
        }

        public Color HealingDoneColor
        {
            get => (Color?) ColorConverter.ConvertFromString(_optionsService.Get(Sections.Ui, nameof(HealingDoneColor), "#A419C819")) ?? Color.FromArgb(164, 25, 200, 25);
            set => SaveOption(Sections.Ui, nameof(HealingDoneColor), value.ToString());
        }

        public Color DamageReceivedColor
        {
            get => (Color?) ColorConverter.ConvertFromString(_optionsService.Get(Sections.Ui, nameof(DamageReceivedColor), "#A4C81919")) ?? Color.FromArgb(164, 200, 25, 25);
            set => SaveOption(Sections.Ui, nameof(DamageReceivedColor), value.ToString());
        }

        public int FontSize
        {
            get => Convert.ToInt32(_optionsService.Get(Sections.Ui, nameof(FontSize), "13"));
            set => SaveOption(Sections.Ui, nameof(FontSize), value.ToString());
        }

        public int IdleDuration
        {
            get => Convert.ToInt32(_optionsService.Get(Sections.Parser, nameof(IdleDuration), "10"));
            set => SaveOption(Sections.Parser, nameof(IdleDuration), value.ToString(CultureInfo.InvariantCulture));
        }

        public string RaidHandle
        {
            get => _optionsService.Get(Sections.Raid, nameof(RaidHandle), Guid.NewGuid().ToPrettyString());
            set => SaveOption(Sections.Raid, nameof(RaidHandle), value);
        }

        public int NotifierTimer
        {
            get => Convert.ToInt32(_optionsService.Get(Sections.AfkNotifier, nameof(NotifierTimer), "9"));
            set => SaveOption(Sections.AfkNotifier, nameof(NotifierTimer), value.ToString());
        }

        public double Top
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(Top), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(Top), value.ToString(CultureInfo.InvariantCulture));
        }

        public double Left
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(Left), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(Left), value.ToString(CultureInfo.InvariantCulture));
        }

        public double TopDamageDone
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(TopDamageDone), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(TopDamageDone), value.ToString(CultureInfo.InvariantCulture));
        }

        public double LeftDamageDone
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(LeftDamageDone), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(LeftDamageDone), value.ToString(CultureInfo.InvariantCulture));
        }

        public double TopHealingDone
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(TopHealingDone), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(TopHealingDone), value.ToString(CultureInfo.InvariantCulture));
        }

        public double LeftHealingDone
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(LeftHealingDone), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(LeftHealingDone), value.ToString(CultureInfo.InvariantCulture));
        }

        public double TopDamageReceived
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(TopDamageReceived), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(TopDamageReceived), value.ToString(CultureInfo.InvariantCulture));
        }

        public double LeftDamageReceived
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(LeftDamageReceived), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(LeftDamageReceived), value.ToString(CultureInfo.InvariantCulture));
        }

        public double TopOpponents
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(TopOpponents), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(TopOpponents), value.ToString(CultureInfo.InvariantCulture));
        }

        public double LeftOpponents
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(LeftOpponents), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(LeftOpponents), value.ToString(CultureInfo.InvariantCulture));
        }

        public string DamageDetailsSortingProperty
        {
            get => _optionsService.Get(Sections.Ui, nameof(DamageDetailsSortingProperty), "Total");
            set => SaveOption(Sections.Ui, nameof(DamageDetailsSortingProperty), value);
        }

        public string DamageDetailsSortingDirection
        {
            get => _optionsService.Get(Sections.Ui, nameof(DamageDetailsSortingDirection), "Ascending");
            set => SaveOption(Sections.Ui, nameof(DamageDetailsSortingDirection), value);
        }

        public string HealingDetailsSortingProperty
        {
            get => _optionsService.Get(Sections.Ui, nameof(HealingDetailsSortingProperty), "Total");
            set => SaveOption(Sections.Ui, nameof(HealingDetailsSortingProperty), value);
        }

        public string HealingDetailsSortingDirection
        {
            get => _optionsService.Get(Sections.Ui, nameof(HealingDetailsSortingDirection), "Ascending");
            set => SaveOption(Sections.Ui, nameof(HealingDetailsSortingDirection), value);
        }

        // todo: test performance
        public string OpponentsSortingProperty
        {
            get => _optionsService.Get(Sections.Ui, nameof(OpponentsSortingProperty), "DamageReceived");
            set => SaveOption(Sections.Ui, nameof(OpponentsSortingProperty), value);
        }

        public string OpponentsSortingDirection
        {
            get => _optionsService.Get(Sections.Ui, nameof(OpponentsSortingDirection), "Ascending");
            set => SaveOption(Sections.Ui, nameof(OpponentsSortingDirection), value);
        }

        public string RaidDamageDoneSortingProperty
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidDamageDoneSortingProperty), "Total");
            set => SaveOption(Sections.Ui, nameof(RaidDamageDoneSortingProperty), value);
        }

        public string RaidDamageDoneSortingDirection
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidDamageDoneSortingDirection), "Ascending");
            set => SaveOption(Sections.Ui, nameof(RaidDamageDoneSortingDirection), value);
        }

        public string RaidHealingDoneSortingProperty
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidHealingDoneSortingProperty), "Total");
            set => SaveOption(Sections.Ui, nameof(RaidHealingDoneSortingProperty), value);
        }

        public string RaidHealingDoneSortingDirection
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidHealingDoneSortingDirection), "Ascending");
            set => SaveOption(Sections.Ui, nameof(RaidHealingDoneSortingDirection), value);
        }

        public string RaidDamageReceivedSortingProperty
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidDamageReceivedSortingProperty), "Total");
            set => SaveOption(Sections.Ui, nameof(RaidDamageReceivedSortingProperty), value);
        }

        public string RaidDamageReceivedSortingDirection
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidDamageReceivedSortingDirection), "Ascending");
            set => SaveOption(Sections.Ui, nameof(RaidDamageReceivedSortingDirection), value);
        }

        public string RaidOpponentsSortingProperty
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidOpponentsSortingProperty), "DamageReceived");
            set => SaveOption(Sections.Ui, nameof(RaidOpponentsSortingProperty), value);
        }

        public string RaidOpponentsSortingDirection
        {
            get => _optionsService.Get(Sections.Ui, nameof(RaidOpponentsSortingDirection), "Ascending");
            set => SaveOption(Sections.Ui, nameof(RaidOpponentsSortingDirection), value);
        }

        public long ProgramLaunches
        {
            get => Convert.ToInt64(_optionsService.Get(Sections.Support, nameof(ProgramLaunches), "0"));
            set => SaveOption(Sections.Support, nameof(ProgramLaunches), value.ToString());
        }

        public bool PatreonAdvertised
        {
            get => bool.Parse(_optionsService.Get(Sections.Support, nameof(PatreonAdvertised), bool.FalseString));
            set => SaveOption(Sections.Support, nameof(PatreonAdvertised), value.ToString());
        }

        private void SaveOption(string section, string propertyName, string value)
        {
            try
            {
                _optionsService.Set(section, propertyName, value);
                RaisePropertyChanged(propertyName);
            }
            catch
            {
                SaveError?.Invoke(this, EventArgs.Empty);
            }
        }

        public void ResetToDefaults()
        {
            ForegroundColor = Colors.White;
            BackgroundColor = Colors.Black;
            PrimaryOpacity = 0.5d;
            SecondaryOpacity = 0.3d;
            FontSize = 13;
            IdleDuration = 10;
            NotifierTimer = 9;
            DamageDoneColor = Color.FromArgb(164, 25, 25, 200);
            HealingDoneColor = Color.FromArgb(164, 25, 200, 25);
            DamageReceivedColor = Color.FromArgb(164, 200, 25, 25);
        }

        public bool ShowDamageDone
        {
            get => GetRaidVisibility(RaidViewMode.DamageDone);
            set => SetRaidVisibility(RaidViewMode.DamageDone, value);
        }

        public bool ShowHealingDone
        {
            get => GetRaidVisibility(RaidViewMode.HealingDone);
            set => SetRaidVisibility(RaidViewMode.HealingDone, value);
        }

        public bool ShowDamageReceived
        {
            get => GetRaidVisibility(RaidViewMode.DamageReceived);
            set => SetRaidVisibility(RaidViewMode.DamageReceived, value);
        }

        public bool ShowOpponents
        {
            get => GetRaidVisibility(RaidViewMode.Opponents);
            set => SetRaidVisibility(RaidViewMode.Opponents, value);
        }

        public bool Topmost
        {
            get => _topmost;
            set => SetProperty(ref _topmost, value);
        }

        public bool CaptureMode
        {
            get => bool.Parse(_optionsService.Get(Sections.Ui, nameof(CaptureMode), bool.FalseString));
            set
            {
                _optionsService.Set(Sections.Ui, nameof(CaptureMode), value.ToString());
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();
            }
        }

        public bool TopmostAlways
        {
            get => bool.Parse(_optionsService.Get(Sections.Ui, nameof(TopmostAlways), bool.TrueString));
            set => SaveOption(Sections.Ui, nameof(TopmostAlways), value.ToString());
        }

        public int MagnetDistance
        {
            get => int.Parse(_optionsService.Get(Sections.Ui, nameof(MagnetDistance), "15"));
            set => SaveOption(Sections.Ui, nameof(MagnetDistance), value.ToString());
        }

        public double MainScaleFactor
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(MainScaleFactor), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(MainScaleFactor), value.ToString(CultureInfo.InvariantCulture));
        }

        public double RaidScaleFactor
        {
            get => Convert.ToDouble(_optionsService.Get(Sections.Ui, nameof(RaidScaleFactor), "100"), CultureInfo.InvariantCulture);
            set => SaveOption(Sections.Ui, nameof(RaidScaleFactor), value.ToString(CultureInfo.InvariantCulture));
        }

        public bool DontSendMyMetrics
        {
            get => bool.Parse(_optionsService.Get(Sections.Ui, nameof(DontSendMyMetrics), bool.FalseString));
            set => _optionsService.Set(Sections.Ui, nameof(DontSendMyMetrics), value.ToString());
        }

        public bool LastRaidFight { get; set; } = true;
        public string Name => "Crowbars";

        public HotkeyBinding GetHotkeyBinding(string name)
        {
            return new HotkeyBinding(name, Hotkey.Parse(_optionsService.Get(Sections.Hotkey, name, null)));
        }

        public void SetHotkeyBinding(string name, Hotkey hotkey)
        {
            _optionsService.Set(Sections.Hotkey, name, hotkey?.ToString());
            OnPropertyChanged(new PropertyChangedEventArgs(name));
        }

        public bool GetRaidVisibility(RaidViewMode viewMode)
        {
            return bool.Parse(_optionsService.Get(Sections.Ui, "Show" + viewMode, bool.TrueString));
        }

        public void SetRaidVisibility(RaidViewMode viewMode, bool value)
        {
            SaveOption(Sections.Ui, "Show" + viewMode, value.ToString());
        }

        public static int GetCompareSign(string direction)
        {
            return direction == "Ascending" ? 1 : -1;
        }

        public void ResetLayout()
        {
            Top = 100;
            Left = 100;

            TopDamageDone = 100;
            LeftDamageDone = 420 + 100;

            TopDamageReceived = 100 + 182;
            LeftDamageReceived = 420 + 100;

            TopHealingDone = 100;
            LeftHealingDone = 420 + 100 + 280;

            TopOpponents = 100 + 182;
            LeftOpponents = 420 + 100 + 280;
        }
    }
}