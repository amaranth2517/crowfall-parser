using System;

namespace OptionsModule
{
    public static class Extensions
    {
        // note: makes Guid easy to copy with double click
        public static string ToPrettyString(this Guid guid)
        {
            var result = Convert.ToBase64String(guid.ToByteArray());
            result = result.Split('=')[0];
            result = result.Replace('+', '\'');
            result = result.Replace('/', '_');
            result = result.Replace('\'', '_');

            return result;
        }
    }
}